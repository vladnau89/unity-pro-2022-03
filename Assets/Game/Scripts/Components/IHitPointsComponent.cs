﻿namespace Game.Scripts.Components
{
    public interface IHitPointsComponent
    {
        int HitPoints { get; }

        void ChangeHitPoints(int delta);
    }
}