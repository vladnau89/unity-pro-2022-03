﻿using UnityEngine;

namespace Game.Scripts.Components
{
    public class MoveComponent : MonoBehaviour, IMoveComponent
    {
        [SerializeField] private Transform moveTransform;
        [field: SerializeField] public float Speed { get; private set; } = 10;

        public Vector3 Position => moveTransform.position;
        public Quaternion Rotation => moveTransform.rotation;

        public void Move(Vector3 direction)
        {
            moveTransform.position += direction * Speed * Time.deltaTime;
        }
    }
}