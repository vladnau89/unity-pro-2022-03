﻿using UnityEngine;

namespace Game.Scripts.Components
{
    public interface IMoveComponent
    {
        float Speed { get; }

        Vector3 Position { get; }

        Quaternion Rotation { get; }

        void Move(Vector3 direction);
    }
}