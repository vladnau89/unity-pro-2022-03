﻿namespace Game.Scripts.Components
{
    public interface IAttackComponent
    {
        int Damage { get; }

        void Attack(IHitPointsComponent hitPointsComponent);
    }
}