﻿using UnityEngine;

namespace Game.Scripts.Components
{
    public class AttackComponent : MonoBehaviour, IAttackComponent
    {
        [field: SerializeField] public int Damage { get; private set; } = 10;
        
        public void Attack(IHitPointsComponent hitPointsComponent)
        {
            Debug.LogWarning("Attack!");
            hitPointsComponent.ChangeHitPoints(-Damage);
        }
    }
}