﻿using System;
using UnityEngine;

namespace Game.Scripts.Components
{
    public class HitPointsComponent : MonoBehaviour, IHitPointsComponent
    {
        [field: SerializeField] public int HitPoints { get; private set; }

        private int _maxHp;
        private int _minHp = 0;
        
        private void Awake()
        {
            _maxHp = HitPoints;
        }

        public void ChangeHitPoints(int delta)
        {
            HitPoints = Mathf.Clamp(HitPoints + delta , _minHp, _maxHp);
        }
    }
}