﻿using System;
using Entities;
using Game.Scripts.Components;
using UnityEngine;

namespace Game.Scripts.Controllers
{
    public class MoveController : MonoBehaviour
    {
        [SerializeField] private MonoEntity entity;
        
        private IMoveComponent _moveComponent;

        private void Awake()
        {
            _moveComponent = entity.Element<IMoveComponent>();
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                _moveComponent.Move(Vector3.forward);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                _moveComponent.Move(Vector3.back);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                _moveComponent.Move(Vector3.left);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                _moveComponent.Move(Vector3.right);
            }
        }
    }
}