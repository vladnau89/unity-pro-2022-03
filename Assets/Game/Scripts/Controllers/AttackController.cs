﻿using System;
using Entities;
using Game.Scripts.Components;
using UnityEngine;

namespace Game.Scripts.Controllers
{
    public class AttackController : MonoBehaviour
    {
        [SerializeField] private MonoEntity entity;

        private IAttackComponent _attackComponent;
        private IHitPointsComponent _hitPointsComponent;

        private void Awake()
        {
            _attackComponent = entity.Element<IAttackComponent>();
            _hitPointsComponent = entity.Element<IHitPointsComponent>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _attackComponent.Attack(_hitPointsComponent); // self attack
            }
        }
    }
}