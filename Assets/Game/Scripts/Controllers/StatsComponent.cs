﻿using System;
using Entities;
using Game.Scripts.Components;
using UnityEngine;

namespace Game.Scripts.Controllers
{
    public class StatsComponent : MonoBehaviour
    {
        [SerializeField] private MonoEntity entity;
        
        private IHitPointsComponent _hitPointsComponent;
        private IMoveComponent _moveComponent;
        private IAttackComponent _attackComponent;

        private void Awake()
        {
            _hitPointsComponent = entity.Element<IHitPointsComponent>();
            _moveComponent = entity.Element<IMoveComponent>();
            _attackComponent = entity.Element<IAttackComponent>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Debug.Log($"Player hp : {_hitPointsComponent.HitPoints}  attack : {_attackComponent.Damage} speed : {_moveComponent.Speed} ");
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Debug.Log($"Player position : {_moveComponent.Position} \t rotation : {_moveComponent.Rotation.eulerAngles} ");
            }
        }
    }
}